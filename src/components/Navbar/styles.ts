import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  height: 3.375rem;
  padding: 1.2rem 1rem;
  margin-bottom: 1rem;
  background-color: var(--red-600);

  @media (max-width: 768px) {
    display: none;
  }

  & > ul {
    display: flex;
    align-items: center;
    list-style: none;
    width: 100%;
    max-width: var(--max-width);
    margin: 0 auto;
    padding: 0 1rem;

    a {
      color: var(--white);
      font-weight: 700;
      text-transform: uppercase;
    }

    li + li {
      margin-left: 5rem;
    }
  }
`
