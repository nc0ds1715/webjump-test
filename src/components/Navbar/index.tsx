import { Container } from "./styles";
import Link from "next/link";

export function Navbar() {
  return (
    <Container>
      <ul>
        <li>
          <Link href='/'>
            <a>Página inicial</a>
          </Link>
        </li>
        <li>
          <Link href='/products/camisetas'>
            <a>Camisetas</a>
          </Link>
        </li>
        <li>
          <Link href='/products/calcas'>
            <a>Calças</a>
          </Link>
        </li>
        <li>
          <Link href='/products/calcados'>
            <a>Calçados</a>
          </Link>
        </li>
        <li>
          <Link href='/'>
            <a>Contato</a>
          </Link>
        </li>
      </ul>
    </Container>
  )
}
