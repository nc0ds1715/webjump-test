import { Container } from "./styles";

interface MobileBtnProps {
  red?: boolean
  onClick: () => void
}

export function MobileBtn({ red, onClick }: MobileBtnProps) {
  return (
    <Container
     red={red}
     type="button"
     onClick={onClick}
    >?</Container>
  )
}
