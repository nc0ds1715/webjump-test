import styled from "styled-components";

interface ContainerProps {
  red?: boolean;
}

export const Container = styled.button<ContainerProps>`
  width: 2rem;
  height: 2rem;
  border: 3px solid ${({ red }) => red ? 'var(--red-600)' : 'var(--black-900)'};
  border-radius: 0.2rem;
  background-color: var(--white);
  color: ${({ red }) => red ? 'var(--red-600)' : 'var(--black-900)'};
  font-size: 1.2rem;
  font-weight: 900;

  @media (min-width: 768px) {
    display: none;
  }
`
