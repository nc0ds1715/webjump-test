import { Container } from "./styles";

interface SearchInputProps {
  mobile?: boolean;
}

export function SearchInput({ mobile }: SearchInputProps) {
  return (
    <Container mobile={mobile}>
      <input type="text" />
      <button type="button">Buscar</button>
    </Container>
  )
}
