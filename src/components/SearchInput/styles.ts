import styled from "styled-components";

interface ContainerProps {
  mobile?: boolean;
}

export const Container = styled.div<ContainerProps>`
  width: 100%;
  max-width: 525px;
  height: 2.75rem;
  display: flex;
  align-items: center;

  @media (max-width: 768px) {
    ${({ mobile }) => !mobile && 'display: none;'}

    flex-direction: column;
    gap: 1rem;
    height: fit-content;
  }

  & > input {
    width: 100%;
    height: 100%;
    padding: 0.5rem;
    border: 1px solid var(--gray-600);

    @media (max-width: 768px) {
      height: 2.75rem;
    }
  }

  & > button {
    width: 6.875rem;
    height: 100%;
    border: 1px solid var(--red-500);
    background-color: var(--red-600);
    color: var(--white);
    text-transform: uppercase;
    font-weight: 900;

    @media (max-width: 768px) {
      width: 100%;
      height: 2.75rem;
    }
  }
`
