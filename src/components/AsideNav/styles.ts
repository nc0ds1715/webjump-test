import styled from "styled-components";

export const Container = styled.aside`
  width: 30%;
  min-height: 490px;
  max-height: 500px;
  padding: 1rem;
  background-color: var(--gray-400);

  @media (max-width: 768px) {
    display: none;
  }
  
  & > nav {
    & > ul {
      list-style-position: inside;

      a {
        color: var(--black-900);
      }
    }
  }
`
