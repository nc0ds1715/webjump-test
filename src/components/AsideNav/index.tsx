import Link from "next/link";
import { Container } from "./styles";

export function AsideNav() {
  return (
    <Container>
      <nav>
        <ul>
          <li>
            <Link href='/'>
              <a>Página inicial</a>
            </Link>
          </li>
          <li>
            <Link href='/products/camisetas'>
              <a>Camisetas</a>
            </Link>
          </li>
          <li>
            <Link href='/products/calcas'>
              <a>Calças</a>
            </Link>
          </li>
          <li>
            <Link href='/products/calcados'>
              <a>Calçados</a>
            </Link>
          </li>
          <li>
            <Link href='/'>
              <a>Contato</a>
            </Link>
          </li>
        </ul>
      </nav>
    </Container>
  )
}
