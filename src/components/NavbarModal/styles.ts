import styled from "styled-components";

export const Container = styled.div`
  width: 100%;

  & > ul {
    list-style-position: inside;

    & > li {
      & + li {
        margin-top: 0.8rem;
      }

      & > a {
        color: var(--black-900);
        font-weight: 700;
      }
    }
  }
`

export const CloseBtn = styled.button`
  width: 2rem;
  height: 2rem;
  border: 3px solid var(--black-900);
  border-radius: 0.2rem;
  background-color: var(--white);
  color: var(--black-900);
  font-size: 1.2rem;
  font-weight: 900;
  position: absolute;
  right: 0.5rem;
  top: 0.5rem;
`
