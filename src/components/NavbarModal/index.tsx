import Link from 'next/link'
import Modal from 'react-modal'
import { CloseBtn, Container } from './styles'

interface NavbarModalProps {
  isOpen: boolean;
  onRequestClose: () => void
}

export function NavbarModal({ isOpen, onRequestClose }: NavbarModalProps) {
  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={onRequestClose}
      className='react-modal'
      overlayClassName='react-modal-overlay'
    >
      <CloseBtn
        type='button'
        onClick={onRequestClose}
      >X</CloseBtn>
      <Container>
        <ul>
          <li>
            <Link href='/'>
              <a onClick={onRequestClose}>Página inicial</a>
            </Link>
          </li>
          <li>
            <Link href='/products/camisetas'>
              <a onClick={onRequestClose}>Camisetas</a>
            </Link>
          </li>
          <li>
            <Link href='/products/calcas'>
              <a onClick={onRequestClose}>Calças</a>
            </Link>
          </li>
          <li>
            <Link href='/products/calcados'>
              <a onClick={onRequestClose}>Calçados</a>
            </Link>
          </li>
          <li>
            <Link href='/'>
              <a onClick={onRequestClose}>Contato</a>
            </Link>
          </li>
        </ul>
      </Container>
    </Modal>
  )
}
