import Modal from 'react-modal'
import { SearchInput } from '../SearchInput'
import { CloseBtn, Container } from './styles'

interface SearchModalProps {
  isOpen: boolean;
  onRequestClose: () => void
}

export function SearchModal({
  isOpen,
  onRequestClose
}: SearchModalProps) {
  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={onRequestClose}
      className='react-modal'
      overlayClassName='react-modal-overlay'
    >
      <CloseBtn
        type='button'
        onClick={onRequestClose}
      >X</CloseBtn>
      <Container>
        <h2>Procure um produto</h2>
        <SearchInput mobile />
      </Container>
    </Modal>
  )
}
