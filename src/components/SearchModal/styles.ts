import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  height: 10rem;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;

  & > h2 {
    margin-bottom: 1rem;
  }
`

export const CloseBtn = styled.button`
  width: 2rem;
  height: 2rem;
  border: 3px solid var(--black-900);
  border-radius: 0.2rem;
  background-color: var(--white);
  color: var(--black-900);
  font-size: 1.2rem;
  font-weight: 900;
  position: absolute;
  right: 0.5rem;
  top: 0.5rem;
`
