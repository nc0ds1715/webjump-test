import styled from "styled-components";

export const Container = styled.aside`
  width: 25%;
  border: 1px solid var(--gray-400);
  padding: 1rem;

  @media (max-width: 600px) {
    width: 100%;
  }

  & > h3 {
    text-transform: uppercase;
    color: var(--red-600);
  }

  & > dl {
    margin-bottom: 1rem;
    
    & > dd {
      font-size: 1rem;
      font-weight: 700;
      text-transform: uppercase;
      margin: 0.5rem 0;
    }

    & > dt {
      & > ul {
        list-style-position: inside;
        width: 100%;

        & > li {
          & > a {
            color: var(--gray-600);
            cursor: pointer;
          }
        }

        &.color-filter {
          list-style: none;
          display: flex;
          gap: 0.2rem;
          flex-wrap: wrap;
        }
      }
    }
  }

  & > a {
    color: var(--black-900);
  }
`

interface ColorBoxProps {
  color: string;
}

interface IColors {
  [key: string]: string
}

const colors: IColors = {
  Preto: '#000',
  Laranja: 'orange',
  Amarela: '#FFF000',
  Rosa: 'pink',
  Bege: '#c4a73b',
  Azul: '#1216ed',
  Cinza: '#DDD'
}

export const ColorBox = styled.div<ColorBoxProps>`
  width: 100%;
  max-width: 3rem;
  min-width: 3rem;
  height: 1.5rem;
  background-color: ${({ color }) => colors[color]};
`
