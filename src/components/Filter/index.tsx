import Link from "next/link";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { api } from "../../services/api";
import { ColorBox, Container } from "./styles";

interface FilterProps {
  product: string;
}

interface IFilters {
  filters: { [key: string]: string };
  values: Array<{ [key: string]: string }>;
}

export function Filter({ product }: FilterProps) {
  const [filters, setFilters] = useState<IFilters>();

  const router = useRouter()

  const getFilters = async () => {
    try {
      const { data } = await api.get(`/${product}`)

      const result = Object.keys(data.filters).map((i: any, index: number) => {
        const items = data.items
        const result = [] as Array<{ [key: string]: string }>

        return { [i]: items.map((val: any) => result[index] = val.filter[i]).filter((val: any, indx: number, arr: any) => arr.indexOf(val) === indx) }
      })

      setFilters({
        filters: data.filters,
        values: result
      })
    } catch(err) {
      console.log(err)
      alert('Algo deu errado')
    }
  }

  useEffect(() => {
    if(product !== undefined) {
      getFilters()
    }
  }, [product])

  return (
    <Container>
      <h3>Filtre por</h3>
      <dl>
        { filters !== undefined && Object.keys(filters.filters).map((item, index) => {
          const items = filters.values[index][item] as unknown as string[]

          return (
            <React.Fragment key={index}>
              <dd>{filters.filters[item]}</dd>
              <dt>
                <ul className={`${item}-filter`}>
                  { items.map(val => item === 'color' ? (
                    <li key={`${val}-list-item-${index}`}>
                      <Link href={
                        `${
                          Object.keys(router.query).includes(item)
                          ? router.asPath.replace(`${item}=${router.query[item]}`, `${item}=${val}`)
                          : (
                            router.asPath.includes('?') ?
                            `${router.asPath}&${item}=${val}`
                            : `${router.asPath}?${item}=${val}`
                          )
                        }`
                      } shallow>
                        <a>
                          <ColorBox
                            color={val}
                          />
                        </a>
                      </Link>
                    </li>
                  ) : (
                    <li key={`${val}-list-item-${index}`}>
                      <Link href={
                        `${
                          Object.keys(router.query).includes(item)
                          ? router.asPath.replace(`${item}=${router.query[item]}`, `${item}=${val}`)
                          : (
                            router.asPath.includes('?') ?
                            `${router.asPath}&${item}=${val}`
                            : `${router.asPath}?${item}=${val}`
                          )
                        }`
                      } shallow>
                        <a>{val}</a>
                      </Link>
                    </li>
                  )) }
                </ul>
              </dt>
            </React.Fragment>
          )
        }) }
      </dl>
      <Link href={`/products/${product}`} shallow>
        <a>Limpar filtro</a>
      </Link>
    </Container>
  )
}
