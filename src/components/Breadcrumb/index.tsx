import Head from 'next/head'
import Link from "next/link";
import { Nav } from "./styles";

interface BreadcrumbProps {
  links: string[]
}

interface LabelsType {
  [key: string]: string;
  products: string;
  calcas: string;
  calcados: string;
}

const labels: LabelsType = {
  products: 'Produtos' as string,
  calcas: 'Calças' as string,
  calcados: 'Calçados' as string
}

export function Breadcrumb({ links }: BreadcrumbProps) {
  const pages = links.filter(item => item.length > 0)

  return (
    <>
      <Head>
        <title>{labels[pages[pages.length - 1]] === undefined ? pages[pages.length - 1].replace(pages[pages.length - 1].charAt(0), pages[pages.length - 1].charAt(0).toUpperCase()) : labels[pages[pages.length - 1]]} | Webjump</title>
      </Head>
      <Nav>
        <ol>
          <li itemScope itemType="http://data-vocabulary.org/Breadcrumb">
            <Link href='/'>
              <a itemProp="url">
                <span itemProp="title">Página inicial</span>
              </a>
            </Link>
          </li>
          { pages.map((item, index) => (
              <li key={index} itemScope itemType="http://data-vocabulary.org/Breadcrumb">
                <Link href={`/${pages.filter((link, ind) => ind <= index).join('/')}`}>
                  <a itemProp="url">
                    <span itemProp="title">{labels[item] === undefined ? item.replace(item.charAt(0), item.charAt(0).toUpperCase()) : labels[item]}</span>
                  </a>
                </Link>
              </li>
            )
          )}
        </ol>
      </Nav>
    </>
  )
}
