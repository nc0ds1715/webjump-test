import styled from "styled-components";

export const Nav = styled.nav`
  margin-bottom: 1rem;

  & > ol {
    list-style: none;
    display: flex;
    flex-wrap: wrap;
    justify-content: flex-start;

    & > li {
      & + li {
        &::before {
          content: '>';
          margin: 0 0.5rem;
          color: var(--gray-600);
        }
      }

      & > a {
        color: var(--green-600);
      }

      &:last-of-type {
        & > a {
          color: var(--red-600);
        }
      }
    }
  }
`
