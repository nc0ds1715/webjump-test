import styled from "styled-components";

export const Container = styled.nav`
  width: 100%;
  margin: 0 auto;
  padding: 0.2rem 1rem;
  background-color: var(--black-900);

  & > p {
    width: 100%;
    max-width: var(--max-width);
    margin: 0 auto;
    text-align: end;
    color: var(--white);
    font-size: 0.875rem;
    font-weight: 300;

    @media (max-width: 768px) {
      text-align: center;
    }

    & > a {
      color: var(--white);
      font-weight: 500;
      text-decoration: underline;
    }
  }
`
