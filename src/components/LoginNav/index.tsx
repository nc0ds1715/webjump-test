import { Container } from "./styles";

export function LoginNav() {
  return (
    <Container>
      <p><a href="#">Acesse sua conta</a> ou <a href="#">Cadastre-se</a></p>
    </Container>
  )
}
