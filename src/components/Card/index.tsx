import { Container } from "./styles";

interface CardProps {
  img: string;
  title: string;
  price: number;
}

export function Card({ img, title, price }: CardProps) {
  return (
    <Container>
      <a>
        <img src={`/${img}`} alt={`Imagem ${title}`} />
        <div>
          <h3>{title}</h3>
          <div>
            <p>{ Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(price) }</p>
            <button type="button">Comprar</button>
          </div>
        </div>
      </a>
    </Container>
  )
}
