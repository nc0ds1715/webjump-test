import styled from "styled-components";

export const Container = styled.article`
  width: 100%;

  & > a {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    color: var(--black-900);
    cursor: pointer;
    height: 22rem;

    & > img {
      width: 100%;
      max-height: 12rem;
      border: 2px solid var(--gray-400);
    }

    & > div {
      width: 100%;

      & > h3 {
        font-size: 1rem;
        font-weight: 300;
        text-align: center;
        text-transform: uppercase;
      }

      & > div {
        width: 100%;
        display: flex;
        flex-direction: column;
        align-items: center;

        & > p {
          font-size: 1.2rem;
          font-weight: 700;
          text-align: center;
          margin-top: 1rem;
          margin-bottom: 0.5rem;
        }
    
        & > button {
          width: 100%;
          max-width: 25rem;
          height: 2.8rem;
          font-size: 1.2rem;
          font-weight: 700;
          text-transform: uppercase;
          color: var(--white);
          background-color: var(--green-600);
          border: none;
          border-radius: 0.5rem;
          transition: 0.1s ease-in-out;

          &:hover {
            filter: brightness(0.8);
          }

          @media (max-width: 600px) {
            display: none;
          }
        }
      }
    }
  }
`
