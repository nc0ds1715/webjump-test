import styled from "styled-components";

export const Container = styled.header`
  width: 100%;
  max-width: var(--max-width);
  margin: 0 auto;
  padding: 2.5rem 1rem;
  display: flex;
  justify-content: space-between;
  align-items: center;
`
