import { Container } from "./styles";
import { SearchInput } from "../SearchInput";
import { MobileBtn } from "../MobileBtn";

interface HeaderProps {
  openSearchModal: () => void;
  openNavbarModal: () => void;
}

export function Header({ openSearchModal, openNavbarModal }: HeaderProps) {
  return (
    <Container>
      <MobileBtn
        onClick={openNavbarModal}
      />
      <img src="/images/webjump-logo.png" alt="webjump" />
      <MobileBtn
        red
        onClick={openSearchModal}
      />
      <SearchInput />
    </Container>
  )
}
