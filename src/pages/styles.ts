import styled from "styled-components";

export const Container = styled.section`
  width: 100%;
  display: flex;
  justify-content: space-between;
  gap: 1rem;

  & > section {
    width: 100%;

    & > div {
      width: 100%;
      height: 15rem;
      background-color: var(--gray-600);
    }

    & > article {
      width: 100%;

      & > h2 {
        font-weight: 400;
        line-height: 3rem;
      }
    }
  }
`
