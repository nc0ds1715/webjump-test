import type { AppProps } from 'next/app'
import { useState } from 'react'
import { Header } from '../components/Header'
import { LoginNav } from '../components/LoginNav'
import { Navbar } from '../components/Navbar'
import Modal from 'react-modal'
import { GlobalStyle } from '../styles/global'
import { SearchModal } from '../components/SearchModal'
import { NavbarModal } from '../components/NavbarModal'

Modal.setAppElement('#__next')

function MyApp({ Component, pageProps }: AppProps) {
  const [searchModalOpen, setSearchModalOpen] = useState<boolean>(false)
  const [navbarModalOpen, setNavbarModalOpen] = useState<boolean>(false)

  const handleOpenSearchModal = () => {
    setSearchModalOpen(true)
  }

  const handleCloseSearchModal = () => {
    setSearchModalOpen(false)
  }

  const handleOpenNavbarModal = () => {
    setNavbarModalOpen(true)
  }

  const handleCloseNavbarModal = () => {
    setNavbarModalOpen(false)
  }

  return (
    <>
      <GlobalStyle />
      <LoginNav />
      <Header
        openSearchModal={handleOpenSearchModal}
        openNavbarModal={handleOpenNavbarModal}
      />
      <Navbar />
      <SearchModal
        isOpen={searchModalOpen}
        onRequestClose={handleCloseSearchModal}
      />
      <NavbarModal
        isOpen={navbarModalOpen}
        onRequestClose={handleCloseNavbarModal}
      />
      <main className='main-container'>
        <Component {...pageProps} />
      </main>
    </>
  )
}

export default MyApp
