import { GetStaticProps, GetStaticPaths } from "next"
import { useRouter } from "next/router"
import { useEffect, useState } from "react"
import { Breadcrumb } from "../../components/Breadcrumb"
import { Filter } from "../../components/Filter"
import { Container } from "./styles/product"
import { BsGrid3X3GapFill } from 'react-icons/bs'
import { FaList } from 'react-icons/fa'
import { Card } from "../../components/Card"
import { api } from "../../services/api"

interface ProductProps {
  productPath: any,
  products: {
    filters: { [key: string]: string },
    items: Array<{ [key: string]: string }>
  }
}

export default function Product({ productPath, products }: ProductProps) {
  const [order, setOrder] = useState('price')
  const [itemsInOrder, setItemsInOrder] = useState<Array<{ [key: string]: string }>>([])
  const [isList, setIsList] = useState(false)
  const [filters, setFilters] = useState<Array<{ [key: string]: string }>>([])
  const [filteredItems, setFilteredItems] = useState<Array<{ [key: string]: string }>>([])

  const router = useRouter()

  const pages = router.pathname.replace('[product]', productPath).split('/')
  
  useEffect(() => {
    if(order === 'price') {
      const orderedItems = [] as Array<{ [key: string]: string }>
      products.items.map((item: any) => item.price)
        .sort((a: any, b: any) => a - b)
        .reverse()
        .map((item: any) => products.items.filter((val: any) => val.price === item))
        .map((item: any) => orderedItems.push(...item))
      return setItemsInOrder(orderedItems)
    }

    if(order === 'name') {
      const orderedItems = [] as Array<{ [key: string]: string }>
      products.items.map(item => item.name).sort().map(item => products.items.filter(val => val.name === item)).map(item => orderedItems.push(...item))
      return setItemsInOrder(orderedItems)
    }
  }, [order, products])

  useEffect(() => {
    const filtersApplied = Object.keys(router.query).filter(item => item !== 'product').map(item => { return { [item]: router.query[item] } }) as Array<{ [key: string]: string }>
    
    setFilters(filtersApplied)
  }, [router])

  useEffect(() => {
    if(filters.length > 0) {
      const arr = [] as Array<Array<{ [key: string]: string }>>
      const filtersNames = [] as Array<{ [key: string]: string }>

      itemsInOrder.forEach((item: any, idxItems: number) => {
        filters.forEach((fil: any, idxFilters: number) => {
          const filNames = Object.keys(fil)
          filtersNames.push(fil)

          if(idxItems === 0) {
            filNames.forEach((obj: any) => {
              arr.push([])
            })
          }
  
          filters.forEach((obj: any) => {
            filNames.forEach((val: any) => {
              if(item.filter[val] === obj[val]) {
                arr[idxFilters].push(item)
              }
            })
          })
        })
      })

      const filtered = [] as Array<{ [key: string]: string }>

      arr.forEach((item: any) => {
        item.forEach((val: any) => {
          filtered.push(val)
        })
      })

      const result = filtered.filter((item: any, index: any) => filtered.indexOf(item) !== index)

      if(filters.length > 1) {
        setFilteredItems(result)
      } else {
        setFilteredItems(filtered)
      }
    }
  }, [filters])

  return (
    <>
      <Breadcrumb
        links={pages}
      />
      <Container>
        { productPath !== '' ? (
          <Filter
            product={productPath}
            key='aside-filter'
          />
        ) : null }
        <section>
          {productPath !== '' && productPath !== undefined && <h1 className="page-title">{productPath.replace(productPath.charAt(0), productPath.charAt(0).toUpperCase())}</h1>}
          <span>
            <span className="icons-box">
              <button onClick={() => setIsList(false)}>
                <BsGrid3X3GapFill className={!isList ? 'active-layout' : undefined} />
              </button>
              <button onClick={() => setIsList(true)}>
                <FaList className={isList ? 'active-layout' : undefined} />
              </button>
            </span>
            <span className="order-box">
              <span>Ordenar por</span>
              <select defaultValue={order} onChange={e => setOrder(e.target.value)}>
                <option value="price">Preço</option>
                <option value="name">Nome</option>
              </select>
            </span>
          </span>
          <main className={isList ? 'list-layout' : undefined}>
            { filters.length === 0 ? itemsInOrder.map((item, index) => (
              <Card
                key={index}
                img={item.image}
                title={item.name}
                price={item.price as unknown as number}
              />
            )) : (
              filteredItems.length > 0 ? filteredItems.map((item, index) => (
                <Card
                key={index}
                img={item.image}
                title={item.name}
                price={item.price as unknown as number}
              />
              )) : <h3>Itens não encontrados!</h3>
            ) }
          </main>
        </section>
      </Container>
      <div className="red-banner"></div>
    </>
  )
}

export const getStaticPaths: GetStaticPaths = async () => {
  const { data } = await api.get('/list')

  return {
    paths: data.map((item: any) => {
      return { params: { product: item.path } }
    }),
    fallback: false
  }
}

export const getStaticProps: GetStaticProps = async ctx => {
  const { data } = await api.get(`/${ctx.params?.product}`)

  return {
    props: {
      productPath: ctx.params?.product,
      products: data
    },
    revalidate: (60 * 60) * 24 //24h
  }
}
