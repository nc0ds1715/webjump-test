import { GetStaticProps } from "next";
import { useRouter } from "next/router";
import Link from "next/link";
import { Breadcrumb } from "../../components/Breadcrumb";
import { api } from "../../services/api";
import { Card, Container } from "./styles/products";
import Head from "next/head";

interface ProductsProps {
  items: Array<{id: number, name: string, path: string}>
}

export default function Products({ items }: ProductsProps) {
  const router = useRouter()

  const pages = router.pathname.split('/')

  return (
    <>
      <Head>
        <title>Produtos | Webjump</title>
      </Head>
      <Breadcrumb
        links={pages}
      />
      <h1 className="page-title">Produtos</h1>
      <Container>
        { items.map((item, index) => (
          <Link href={`/products/${item.path}`} key={index}>
            <a>
              <Card type="button">{item.name}</Card>
            </a>
          </Link>
        )) }
      </Container>
      <div className="red-banner"></div>
    </>
  )
}

export const getStaticProps: GetStaticProps = async () => {
  const { data } = await api.get('/list')

  return {
    props: {
      items: data
    },
    revalidate: 60 * 60 * 24 //24h
  }
}
