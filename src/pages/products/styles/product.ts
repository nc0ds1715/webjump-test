import styled from "styled-components";

export const Container = styled.section`
  width: 100%;
  display: flex;
  align-items: flex-start;
  gap: 1rem;

  @media (max-width: 600px) {
    flex-direction: column;
  }

  & > section {
    width: 100%;

    & > span {
      display: flex;
      justify-content: space-between;
      align-items: center;

      & > .icons-box {
        @media (max-width: 600px) {
          display: none;
        }

        & > button {
          border: none;
          background-color: transparent;

          & + button {
            margin-left: 0.5rem;
          }

          & > svg {
            color: var(--green-600);
            font-size: 1.3rem;

            &.active-layout {
              color: var(--red-600);
            }
          }
        }
      }
      
      & > .order-box {
        width: 50%;
        color: var(--gray-600);
        font-size: 0.8rem;
        text-transform: uppercase;
        font-weight: 700;
        display: flex;
        justify-content: flex-end;
        align-items: center;

        @media (max-width: 600px) {
          width: 100%;
          text-align: right;
        }
        
        & > select {
          width: 50%;
          max-width: 10rem;
          height: 1.8rem;
          margin-left: 0.5rem;
          padding: 0 0.5rem;
          border: 1px solid var(--gray-400);
          border-radius: 0.2rem;
          background-color: var(--white);
        }
      }
    }

    & > main {
      width: 100%;
      margin-top: 2rem;
      display: flex;
      flex-wrap: wrap;
      gap: 1rem;

      & > article {
        max-width: calc((100% / 4) - 0.75rem);

        @media (max-width: 768px) {
          max-width: calc((100% / 3) - 0.75rem);
        }

        @media (max-width: 600px) {
          max-width: calc((100% / 2) - 0.75rem);

          & > a {
            height: fit-content;
            min-height: 16rem;

            & > img {
              max-height: 18rem;
            }
          }
        }
      }

      &.list-layout {
        width: 100%;
        margin-top: 2rem;
        display: flex;
        flex-direction: column;
        gap: 2rem;

        @media (max-width: 600px) {
          width: 100%;
          margin-top: 2rem;
          display: flex;
          flex-direction: row;
          flex-wrap: wrap;
          gap: 1rem;
        }

        & > article {
          max-width: 100%;

          @media (max-width: 600px) {
            max-width: calc((100% / 2) - 0.75rem);
          }

          & > a {
            display: flex;
            flex-direction: row;
            justify-content: flex-start;
            gap: 1rem;
            height: fit-content;

            @media (max-width: 600px) {
              flex-direction: column;
              min-height: 16rem;
              justify-content: space-between;
            }

            & > img {
              max-width: 13.5rem;

              @media (max-width: 600px) {
                max-height: 18rem;
              }
            }

            & > div {
              display: flex;
              flex-direction: column;
              justify-content: center;
            }
          }
        }
      }
    }
  }
`
