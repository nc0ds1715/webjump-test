import styled from "styled-components";

export const Container = styled.section`
  width: 100%;
  margin-top: 1rem;
  display: flex;
  flex-wrap: wrap;
  gap: 1rem;
`

export const Card = styled.button`
  width: 10rem;
  height: 10rem;
  border: 2px solid var(--red-600);
  border-radius: 0.2rem;
  background-color: var(--white);
  font-size: 1.2rem;
  font-weight: 700;
  font-style: italic;
  text-transform: capitalize;
  transition: 0.2s ease-in-out;

  &:hover {
    filter: brightness(0.9);
  }
`
