import { NextApiRequest, NextApiResponse } from "next";

export default function (req: NextApiRequest, res: NextApiResponse) {
  if(req.method !== 'GET') {
    return res.status(405).json('Method Not Allowed')
  }

  const calcados = {
    "filters": {
      "color": "Cor"
    },
    "items": [
      {
        "id": 31,
        "sku": "sku-31",
        "path": "tenis-preto-couro",
        "name": "Tênis Preto Couro",
        "image": "images/shoes-1.jpg",
        "price": 129.9,
        "filter": {
          "color": "Preto"
        }
      },
      {
        "id": 32,
        "sku": "sku-32",
        "path": "tenis-preto",
        "name": "Tênis Preto Masculino",
        "image": "images/shoes-2.jpg",
        "price": 140.5,
        "filter": {
          "color": "Laranja"
        }
      },
      {
        "id": 33,
        "sku": "sku-33",
        "path": "tenis-corrida",
        "name": "Tênis Corrida",
        "image": "images/shoes-3.jpg",
        "price": 261.49,
        "filter": {
          "color": "Amarela"
        }
      },
      {
        "id": 34,
        "sku": "sku-34",
        "path": "tenis-corrida-gel",
        "name": "Tênis Corrida Gel",
        "image": "images/shoes-4.jpg",
        "price": 352.0,
        "filter": {
          "color": "Cinza"
        }
      },
      {
        "id": 35,
        "sku": "sku-35",
        "path": "tenis-slip",
        "name": "Tênis Slip Azul",
        "image": "images/shoes-5.jpg",
        "price": 92.0,
        "filter": {
          "color": "Azul"
        }
      },
      {
        "id": 36,
        "sku": "sku-36",
        "path": "tenis-slip-preto",
        "name": "Tênis Slip Preto",
        "image": "images/shoes-6.jpg",
        "price": 90.0,
        "specialPrice": 50.9,
        "filter": {
          "color": "Preto"
        }
      },
      {
        "id": 37,
        "sku": "sku-37",
        "path": "tenis-corrida-rosa",
        "name": "Tênis Corrida - Rosa ",
        "image": "images/shoes-7.jpg",
        "price": 315.0,
        "filter": {
          "color": "Rosa"
        }
      },
      {
        "id": 38,
        "sku": "sku-38",
        "path": "tenis-rosa",
        "name": "Tênis Rosa Feminino",
        "image": "images/shoes-8.jpg",
        "price": 281.0,
        "filter": {
          "color": "Rosa"
        }
      },
      {
        "id": 39,
        "sku": "sku-39",
        "path": "tenis-casual",
        "name": "Tênis Casual Bege",
        "image": "images/shoes-9.jpg",
        "price": 198.0,
        "specialPrice": 180.9,
        "filter": {
          "color": "Bege"
        }
      },
      {
        "id": 40,
        "sku": "sku-40",
        "path": "tenis-corrida-feminino-azul",
        "name": "Tênis Corrida Feminino Azul",
        "image": "images/shoes-10.jpg",
        "price": 329.0,
        "filter": {
          "color": "Azul"
        }
      }
    ]
  }

  return res.json(calcados)
}
