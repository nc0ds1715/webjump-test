import { NextApiRequest, NextApiResponse } from "next";

export default function (req: NextApiRequest, res: NextApiResponse) {
  if(req.method !== 'GET') {
    return res.status(405).json('Method Not Allowed')
  }

  const camisetas = {
    "filters": {
      "color": "Cor",
      "size": "Tamanho"
    },
    "items": [
      {
        "id": 1,
        "sku": "sku-1",
        "path": "camiseta-preta",
        "name": "Camiseta Preta",
        "image": "images/shirt-1.jpg",
        "price": 59.9,
        "filter": {
          "color": "Preto",
          "size": "Pequena"
        }
      },
      {
        "id": 2,
        "sku": "sku-2",
        "path": "camiseta-laranja",
        "name": "Camiseta Laranja",
        "image": "images/shirt-2.jpg",
        "price": 40.5,
        "filter": {
          "color": "Laranja",
          "size": "Pequena"
        }
      },
      {
        "id": 3,
        "sku": "sku-3",
        "path": "camiseta-amarela",
        "name": "Camiseta Amarela",
        "image": "images/shirt-3.jpg",
        "price": 61.49,
        "filter": {
          "color": "Amarela",
          "size": "Grande"
        }
      },
      {
        "id": 4,
        "sku": "sku-4",
        "path": "camiseta-rosa",
        "name": "Camiseta Rosa",
        "image": "images/shirt-4.jpg",
        "price": 52.0,
        "filter": {
          "color": "Rosa",
          "size": "Grande"
        }
      }
    ]
  }

  return res.json(camisetas)
}
