import { NextApiRequest, NextApiResponse } from "next";

export default function (req: NextApiRequest, res: NextApiResponse) {
  if(req.method !== 'GET') {
    return res.status(405).json('Method Not Allowed')
  }

  const calcas = {
    "filters": {
      "gender": "Gênero"
    },
    "items": [
      {
        "id": 10,
        "sku": "sku-10",
        "path": "calca-social",
        "name": "Calça Social",
        "image": "images/pants-1.jpg",
        "price": 39.9,
        "specialPrice": 29.9,
        "filter": {
          "gender": "Masculina"
        }
      },
      {
        "id": 11,
        "sku": "sku-11",
        "path": "calca-feminina",
        "name": "Calça Feminina",
        "image": "images/pants-2.jpg",
        "price": 24.15,
        "filter": {
          "gender": "Feminina"
        }
      },
      {
        "id": 12,
        "sku": "sku-12",
        "path": "calca-la-feminina",
        "name": "Calça de Lã Feminina",
        "image": "images/pants-3.jpg",
        "price": 61.49,
        "filter": {
          "gender": "Feminina"
        }
      },
      {
        "id": 13,
        "sku": "sku-13",
        "path": "calca-caminhada-masculina",
        "name": "Calça Caminhada Masculina",
        "image": "images/pants-4.jpg",
        "price": 70.0,
        "specialPrice": 58,
        "filter": {
          "gender": "Masculina"
        }
      },
      {
        "id": 14,
        "sku": "sku-14",
        "path": "calca-caminhada-feminina",
        "name": "Calça Caminhada Feminina",
        "image": "images/pants-5.jpg",
        "price": 70.0,
        "filter": {
          "gender": "Feminina"
        }
      }
    ]
  }

  return res.json(calcas)
}
