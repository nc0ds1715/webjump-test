import { NextApiRequest, NextApiResponse } from "next";

export default function (req: NextApiRequest, res: NextApiResponse) {
  if(req.method !== 'GET') {
    return res.status(405).json('Method Not Allowed')
  }

  return res.json([
    {
      "id": 1,
      "name": "Camisetas",
      "path": "camisetas"
    },
    {
      "id": 2,
      "name": "Calças",
      "path": "calcas"
    },
    {
      "id": 3,
      "name": "Calçados",
      "path": "calcados"
    }
  ])
}
