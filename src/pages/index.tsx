import Head from 'next/head'

import { AsideNav } from '../components/AsideNav'
import { Container } from './styles'

export default function Home() {
  return (
    <>
      <Head>
        <title>Webjump - Página inicial</title>
      </Head>
      <Container>
        <AsideNav />
        <section>
          <div></div>
          <article>
            <h2>Seja bem-vindo!</h2>
            <p>Ut culpa ad duis ex Lorem sit. Ullamco elit ullamco non minim proident cillum qui elit velit nulla aliquip in magna elit. Veniam deserunt aute magna quis id incididunt consectetur nostrud officia sunt ex id deserunt. Esse eiusmod nulla aliquip eiusmod sint cillum elit adipisicing adipisicing. Sint eiusmod laborum proident officia elit tempor id officia et.Commodo pariatur ullamco mollit anim ullamco pariatur adipisicing aliqua voluptate duis aliqua adipisicing. Minim fugiat consequat cupidatat aute nulla aute ullamco. Minim culpa duis dolor Lorem voluptate tempor ad voluptate ullamco elit aliqua cupidatat cillum.Deserunt reprehenderit occaecat deserunt fugiat.Laboris cillum sint quis aute.Ullamco officia esse irure id. Tempor Lorem labore exercitation laborum amet sit laborum cupidatat ex amet. Officia voluptate in magna commodo laborum consectetur. Dolor cupidatat esse cillum anim ullamco proident est amet excepteur in mollit.</p>
          </article>
        </section>
      </Container>
      <div className='red-banner'></div>
    </>
  )
}
