import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
  :root {
    --white: #FFF;
    --black-900: #000;
    --black-800: #1A1818;
    --black-700: #231F20;
    --gray-400: #E2DEDC;
    --gray-500: #ACACAC;
    --gray-600: #959595;
    --red-600: #CB0D1F;
    --green-600: #00A8A9;

    --max-width: 1200px;
  }

  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  html,
  body {
    width: 100vw;
    min-height: 100vh;
    overflow-x: hidden;
    -webkit-font-smoothing: antialiased;
  }

  html {
    @media (max-width: 1080px) {
      font-size: 93.75%;
    }

    @media (max-width: 720px) {
      font-size: 87.5%;
    }
  }

  body,
  input,
  textarea,
  button {
    font-family: 'Open Sans', 'Helvetica Neue', sans-serif;
    font-weight: 400;
  }

  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  strong,
  b {
    font-weight: 700;
  }

  a {
    text-decoration: none;
  }

  button {
    cursor: pointer;
  }

  [disabled] {
    opacity: 0.6;
    cursor: not-allowed;
  }

  .main-container {
    width: 100%;
    max-width: var(--max-width);
    margin: 0 auto;
    padding: 0 1rem;
  }

  .red-banner {
    width: 100%;
    height: 11rem;
    margin-top: 1rem;
    background-color: var(--red-600);
  }

  .page-title {
    font-size: 2rem;
    font-weight: 400;
    color: var(--red-600);
    position: relative;
    margin-bottom: 1rem;

    &::after {
      content: '';
      height: 1px;
      background-color: var(--gray-400);
      position: absolute;
      bottom: 0;
      left: 0;
      right: 0;
    }
  }

  .react-modal-overlay {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #00000099;
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .react-modal {
    width: calc(100% - 2rem);
    min-height: 200px;
    max-width: 580px;
    max-height: 98%;
    padding: 3rem 0.5rem 0.5rem;
    background-color: var(--white);
    border-radius: 0.25rem;
    overflow-x: hidden;
    position: relative;
  }
`
