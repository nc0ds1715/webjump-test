# Webjump test

This is a selection process test proposed by Webjump.

## Pages working

- Página inicial
- Produtos
- Produto

!["Homepage image"](./images/home.png)
!["Product page image"](./images/product.png)

## Tecnologies used

- Next.js (React framework)
- Styled-components
- Axios
- TypeScript
- React-Modal
